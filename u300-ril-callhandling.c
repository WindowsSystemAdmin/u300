/* ST-Ericsson U300 RIL
**
** Copyright (C) ST-Ericsson AB 2008-2009
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
**
** Based on reference-ril by The Android Open Source Project.
**
** Heavily modified for ST-Ericsson U300 modems.
** Author: Christian Bejram <christian.bejram@stericsson.com>
*/

#include <stdio.h>
#include <telephony/ril.h>

#include <assert.h>

#include "atchannel.h"
#include "at_tok.h"
#include "u300-ril.h"

#define LOG_TAG "RIL"
#include <utils/Log.h>

/* Last call fail cause, obtained by *ECAV. */
static int s_lastCallFailCause = CALL_FAIL_ERROR_UNSPECIFIED;

static int clccStateToRILState(int state, RIL_CallState * p_state)
{
    switch (state) {
    case 0:
        *p_state = RIL_CALL_ACTIVE;
        return 0;
    case 1:
        *p_state = RIL_CALL_HOLDING;
        return 0;
    case 2:
        *p_state = RIL_CALL_DIALING;
        return 0;
    case 3:
        *p_state = RIL_CALL_ALERTING;
        return 0;
    case 4:
        *p_state = RIL_CALL_INCOMING;
        return 0;
    case 5:
        *p_state = RIL_CALL_WAITING;
        return 0;
    default:
        return -1;
    }
}

/**
 * Note: Directly modified line and has *p_call point directly into
 * modified line.
 */
static int callFromCLCCLine(char *line, RIL_Call * p_call)
{
    /* +CLCC: 1,0,2,0,0,\"+18005551212\",145
       index,isMT,state,mode,isMpty(,number,TOA)? */
    int err;
    int state;
    int mode;

    err = at_tok_start(&line);
    if (err < 0)
        goto error;

    err = at_tok_nextint(&line, &(p_call->index));
    if (err < 0)
        goto error;

    err = at_tok_nextbool(&line, &(p_call->isMT));
    if (err < 0)
        goto error;

    err = at_tok_nextint(&line, &state);
    if (err < 0)
        goto error;

    err = clccStateToRILState(state, &(p_call->state));
    if (err < 0)
        goto error;

    err = at_tok_nextint(&line, &mode);
    if (err < 0)
        goto error;

    p_call->isVoice = (mode == 0);

    err = at_tok_nextbool(&line, &(p_call->isMpty));
    if (err < 0)
        goto error;

    if (at_tok_hasmore(&line)) {
        err = at_tok_nextstr(&line, &(p_call->number));

        /* Tolerate null here */
        if (err < 0)
            return 0;

        err = at_tok_nextint(&line, &p_call->toa);
        if (err < 0)
            goto error;
    }

    return 0;

error:
    LOGE("invalid CLCC line\n");
    return -1;
}

/**
 * AT*ECAV handler function.
 */
void onECAVReceived(const char *s)
{
    char *line;
    char *tok;
    int err;
    int res;

    tok = line = strdup(s);

    err = at_tok_start(&tok);
    if (err < 0)
        goto error;

    /* This is CID, we ignore it since android won't care. */
    err = at_tok_nextint(&tok, &res);
    if (err < 0)
        goto error;

    /* Read ccstate. */
    err = at_tok_nextint(&tok, &res);
    if (err < 0)
        goto error;

    /* If 0 (IDLE), we want to know why. */
    if (res == 0) {
        /* Read call type and process id, we don't care about them though. */
        err = at_tok_nextint(&tok, &res);
        if (err < 0)
            goto error;

        err = at_tok_nextint(&tok, &res);
        if (err < 0)
            goto error;

        /* Now, read exit cause and save it for later. */
        err = at_tok_nextint(&tok, &s_lastCallFailCause);
        if (err < 0)
            goto error;
    }

finally:
    free(line);

    /* Send the response even if we failed.. */
    RIL_onUnsolicitedResponse(RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED, NULL,
                              0);

    return;

error:
    LOGE("ECAV: Failed to parse %s.", s);
    goto finally;
}

/**
 * RIL_REQUEST_HANGUP_WAITING_OR_BACKGROUND
 *
 * Hang up waiting or held (like AT+CHLD=0)
*/
void requestHangupWaitingOrBackground(void *data, size_t datalen,
                                      RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int err;

    /* 3GPP 22.030 6.5.5
       "Releases all held calls or sets User Determined User Busy
        (UDUB) for a waiting call." */
    err = at_send_command("AT+CHLD=0", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_HANGUP_FOREGROUND_RESUME_BACKGROUND
 *
 * Hang up waiting or held (like AT+CHLD=1)
*/
void requestHangupForegroundResumeBackground(void *data, size_t datalen,
                                             RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int err;

    /* 3GPP 22.030 6.5.5
       "Releases all active calls (if any exist) and accepts
        the other (held or waiting) call." */
    err = at_send_command("AT+CHLD=1", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_SWITCH_WAITING_OR_HOLDING_AND_ACTIVE
 *
 * Switch waiting or holding call and active call (like AT+CHLD=2)
*/
void requestSwitchWaitingOrHoldingAndActive(void *data, size_t datalen,
                                            RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int err;

    /* 3GPP 22.030 6.5.5
       "Places all active calls (if any exist) on hold and accepts
        the other (held or waiting) call." */
    err = at_send_command("AT+CHLD=2", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_CONFERENCE
 *
 * Conference holding and active (like AT+CHLD=3)
*/
void requestConference(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int err;

    /* 3GPP 22.030 6.5.5
       "Adds a held call to the conversation." */
    err = at_send_command("AT+CHLD=3", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_SEPARATE_CONNECTION
 *
 * Separate a party from a multiparty call placing the multiparty call
 * (less the specified party) on hold and leaving the specified party 
 * as the only other member of the current (active) call
 *
 * Like AT+CHLD=2x
 *
 * See TS 22.084 1.3.8.2 (iii)
 * TS 22.030 6.5.5 "Entering "2X followed by send"
 * TS 27.007 "AT+CHLD=2x"
*/
void requestSeparateConnection(void *data, size_t datalen, RIL_Token t)
{
    char cmd[12];
    int party = ((int *) data)[0];
    int err;
    ATResponse *atresponse = NULL;

    /* Make sure that party is a single digit. */
    if (party < 1 || party > 9)
        goto error;

    sprintf(cmd, "AT+CHLD=2%d", party);
    err = at_send_command(cmd, &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_EXPLICIT_CALL_TRANSFER
 *
 * Connects the two calls and disconnects the subscriber from both calls.
*/
void requestExplicitCallTransfer(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int err;

    /* 3GPP TS 22.091
       Connects the two calls and disconnects the subscriber from both calls. */
    err = at_send_command("AT+CHLD=4", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_UDUB
 *
 * Send UDUB (user determined used busy) to ringing or 
 * waiting call answer (RIL_BasicRequest r).
*/
void requestUDUB(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int err;

    err = at_send_command("ATH", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_SET_MUTE
 *
 * Turn on or off uplink (microphone) mute.
 *
 * Will only be sent while voice call is active.
 * Will always be reset to "disable mute" when a new voice call is initiated.
*/
void requestSetMute(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int mute = ((int *) data)[0];
    int err = 0;
    char *cmd = NULL;

    assert(mute == 0 || mute == 1);

    asprintf(&cmd, "AT+CMUT=%d", mute);
    at_send_command(cmd, &atresponse);
    free(cmd);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_GET_MUTE
 *
 * Queries the current state of the uplink mute setting.
*/
void requestGetMute(void *data, size_t datalen, RIL_Token t)
{
    char *line = NULL;
    int err = 0;
    int response = 0;
    ATResponse *atresponse;

    err = at_send_command_singleline("AT+CMUT?", "+CMUT:", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    line = atresponse->p_intermediates->line;

    err = at_tok_start(&line);
    if (err < 0)
        goto error;

    err = at_tok_nextint(&line, &response);
    if (err < 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, &response, sizeof(int));

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_LAST_CALL_FAIL_CAUSE
 *
 * Requests the failure cause code for the most recently terminated call.
*
 * See also: RIL_REQUEST_LAST_PDP_FAIL_CAUSE
 */
void requestLastCallFailCause(void *data, size_t datalen, RIL_Token t)
{
    RIL_onRequestComplete(t, RIL_E_SUCCESS, &s_lastCallFailCause,
                          sizeof(int));
}

static void sendCallStateChanged(void *param)
{
    RIL_onUnsolicitedResponse(RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
                              NULL, 0);
}

/**
 * RIL_REQUEST_GET_CURRENT_CALLS 
 *
 * Requests current call list.
*/
void requestGetCurrentCalls(void *data, size_t datalen, RIL_Token t)
{
    int err;
    ATResponse *atresponse;
    ATLine *cursor;
    int countCalls;
    int countValidCalls;
    RIL_Call *calls;
    RIL_Call **response;
    int i;

    err = at_send_command_multiline("AT+CLCC", "+CLCC:", &atresponse);

    if (err != 0 || atresponse->success == 0) {
        RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
        return;
    }

    /* Count the calls. */
    for (countCalls = 0, cursor = atresponse->p_intermediates;
         cursor != NULL; cursor = cursor->p_next) {
        countCalls++;
    }

    /* Yes, there's an array of pointers and then an array of structures. */
    response = (RIL_Call **) alloca(countCalls * sizeof(RIL_Call *));
    calls = (RIL_Call *) alloca(countCalls * sizeof(RIL_Call));
    memset(calls, 0, countCalls * sizeof(RIL_Call));

    /* Init the pointer array. */
    for (i = 0; i < countCalls; i++) {
        response[i] = &(calls[i]);
    }

    for (countValidCalls = 0, cursor = atresponse->p_intermediates;
         cursor != NULL; cursor = cursor->p_next) {
        err = callFromCLCCLine(cursor->line, calls + countValidCalls);

        if (err != 0) {
            continue;
        }

        countValidCalls++;
    }

    RIL_onRequestComplete(t, RIL_E_SUCCESS, response,
                          countValidCalls * sizeof(RIL_Call *));

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/** 
 * RIL_REQUEST_DIAL
 *
 * Initiate voice call.
*/
void requestDial(void *data, size_t datalen, RIL_Token t)
{
    RIL_Dial *dial;
    ATResponse *atresponse = NULL;
    char *cmd;
    const char *clir;
    int err;

    dial = (RIL_Dial *) data;

    switch (dial->clir) {
    case 1:
        clir = "I";
        break;                  /* Invocation */
    case 2:
        clir = "i";
        break;                  /* Suppression */
    default:
    case 0:
        clir = "";
        break;                  /* Subscription default */
    }

    asprintf(&cmd, "ATD%s%s;", dial->address, clir);

    err = at_send_command(cmd, &atresponse);

    free(cmd);

    if (err < 0 || atresponse->success == 0)
        goto error;

    /* Success or failure is ignored by the upper layer here,
       it will call GET_CURRENT_CALLS and determine success that way. */
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_ANSWER
 *
 * Answer incoming call.
 *
 * Will not be called for WAITING calls.
 * RIL_REQUEST_SWITCH_WAITING_OR_HOLDING_AND_ACTIVE will be used in this case
 * instead.
*/
void requestAnswer(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int err;

    err = at_send_command("ATA", &atresponse);

    if (err < 0 || atresponse->success == 0)
        goto error;

    /* Success or failure is ignored by the upper layer here,
       it will call GET_CURRENT_CALLS and determine success that way. */
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_HANGUP
 *
 * Hang up a specific line (like AT+CHLD=1x).
*/
void requestHangup(void *data, size_t datalen, RIL_Token t)
{
    int cid;
    char *cmd = NULL;
    ATLine *cursor;
    int err;
    int i;
    int countCalls;
    ATResponse *atresponse;
    RIL_Call *calls;

    cid = ((int *) data)[0];

    /* 
     * Until we get some silver bullet AT-command that will kill whatever
     * call we have, we need to check what state we're in and act accordingly.
     *
     * TODO: Refactor this and merge with the other query to CLCC.
     */
    err = at_send_command_multiline("AT+CLCC", "+CLCC:", &atresponse);

    if (err != 0 || atresponse->success == 0) {
        at_response_free(atresponse);
        goto error;
    }

    /* Count the calls. */
    for (countCalls = 0, cursor = atresponse->p_intermediates;
         cursor != NULL; cursor = cursor->p_next) {
        countCalls++;
    }

    if (countCalls <= 0)
        goto error;

    calls = (RIL_Call *) alloca(countCalls * sizeof(RIL_Call));
    memset(calls, 0, countCalls * sizeof(RIL_Call));

    for (i = 0, cursor = atresponse->p_intermediates; cursor != NULL;
         cursor = cursor->p_next) {
        err = callFromCLCCLine(cursor->line, calls + i);

        if (err != 0) {
            continue;
        }

        if (calls[i].index == cid)
            break;

        i++;
    }

    at_response_free(atresponse);
    atresponse = NULL;

    /* We didn't find the call. Just drop the request and let android decide. */
    if (calls[i].index != cid)
        goto error;

    if (calls[i].state == RIL_CALL_DIALING ||
        calls[i].state == RIL_CALL_ALERTING) {
        asprintf(&cmd, "ATH");
    } else {
        /* 3GPP 22.030 6.5.5
           "Releases a specific active call X" */
        asprintf(&cmd, "AT+CHLD=1%d", cid);
    }

    err = at_send_command(cmd, &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    if (cmd)
        free(cmd);
    at_response_free(atresponse);
    return;

error:
    /* Success or failure is ignored by the upper layer here,
       it will call GET_CURRENT_CALLS and determine success that way. */
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_DTMF
 *
 * Send a DTMF tone
 *
 * If the implementation is currently playing a tone requested via
 * RIL_REQUEST_DTMF_START, that tone should be cancelled and the new tone
 * should be played instead.
*/
void requestDTMF(void *data, size_t datalen, RIL_Token t)
{
    char c = ((char *) data)[0];
    char *cmd = NULL;
    ATResponse *atresponse = NULL;
    int err = 0;

    /* Set duration to default (manufacturer specific, 70ms in our case). */
    err = at_send_command("AT+VTD=0", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;


    at_response_free(atresponse);
    atresponse = NULL;

    asprintf(&cmd, "AT+VTS=%c", (int) c);
    err = at_send_command(cmd, &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    if (cmd != NULL)
        free(cmd);
    at_response_free(atresponse);

    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_DTMF_START
 *
 * Start playing a DTMF tone. Continue playing DTMF tone until 
 * RIL_REQUEST_DTMF_STOP is received .
 *
 * If a RIL_REQUEST_DTMF_START is received while a tone is currently playing,
 * it should cancel the previous tone and play the new one.
 *
 * See also: RIL_REQUEST_DTMF, RIL_REQUEST_DTMF_STOP.
 */
void requestDTMFStart(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *atresponse = NULL;
    char c = ((char *) data)[0];
    char *cmd = NULL;
    int err = 0;

    /* Set duration to maximum, 10000000  n/10 ms = 10000s. */
    err = at_send_command("AT+VTD=10000000", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    at_response_free(atresponse);
    atresponse = NULL;

    /* Start the DTMF tone. */
    asprintf(&cmd, "AT+VTS=%c", (int) c);
    err = at_send_command(cmd, &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    if (cmd != NULL)
        free(cmd);

    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * RIL_REQUEST_DTMF_STOP
 *
 * Stop playing a currently playing DTMF tone.
 *
 * See also: RIL_REQUEST_DTMF, RIL_REQUEST_DTMF_START.
 */
void requestDTMFStop(void *data, size_t datalen, RIL_Token t)
{
    int err = 0;
    ATResponse *atresponse = NULL;

    err = at_send_command("AT+VTD=0", &atresponse);
    if (err < 0 || atresponse->success == 0)
        goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}
