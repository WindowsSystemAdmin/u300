
  ST-Ericsson U300 series Android Radio Interface Layer

BUILDING
  Building u300-ril is done through the Android build system.

  First, unpack the u300-ril package into somewhere in the android source tree,
  here we have used mydroid/hardware/ste/ as an example.

  In the Android root directory (here called mydroid) do:

  mydroid$ source build/envsetup.sh
  mydroid/hardware/ste/u300-ril$ cd hardware/ste/u300-ril/
  mydroid/hardware/ste/u300-ril$ mm

  This will produce the libu300-ril.so in the 
   mydroid/out/product/..../system/lib/ directory.

INSTALLATION
  Copy the libu300-ril.so file to the /system/lib/ directory on target and
  modify the ril-daemon service in android's init.rc to use it. A sample 
  follows:

  service ril-daemon /system/bin/rild -l /system/lib/libu300-ril.so -- -d /dev/chnlat10 -x /dev/chnlat11 -i caif0
      socket rild stream 660 root radio
      socket rild-debug stream 660 radio system
      user root
      group radio cache inet misc
      disabled

  The command line configuration parameters for u300-ril follows:
    -d : Defines the primary AT channel for the RIL to use. Mandatory.
    -x : Defines a priority AT channel for the RIL to use.
    -i : Defines what network interface will be used for PDP context setup.
         Default is currently caif0.

  The service is marked as disabled since we usually need to configure the
  modem communication channels before starting the RIL. This is done trough
  a simple shell script, combined with a service defined in init.rc that will
  invoke the script. Once the script is finished, the RIL-daemon will be '
  started. A sample service in init.rc:

    service ste-init /system/etc/init.ste.sh
        oneshot

  The init.ste.sh script is shipped with the RIL in the scripts/ folder.

  For device configuration in init.rc, please add:
    
    device /dev/chnlat* 0660 radio radio

  to init.rc above the service definitions. This is important, since the RIL
  will not be able to open the AT channels without the correct permissions set.

  Also, be sure that PPP is disabled in init.rc, since ST-Ericsson RIL does
  not use PPP and if PPP is enabled, setup of data operations will fail.

PATCHES FOR ANDROID
  A few patches needs to be applied to Android for the Data Call Setup to work
  properly. This is due to hardcoded values in Android. The following changes
  are needed:

project frameworks/base/
diff --git a/core/java/android/net/MobileDataStateTracker.java b/core/java/android/net/MobileDataStateTracker.java
index 1d939e1..ef01567 100644
--- a/core/java/android/net/MobileDataStateTracker.java
+++ b/core/java/android/net/MobileDataStateTracker.java
@@ -50,6 +50,8 @@ public class MobileDataStateTracker extends NetworkStateTracker {
     private Phone.DataState mMobileDataState;
     private ITelephony mPhoneService;
     private static final String[] sDnsPropNames = {
+          "net.caif0.dns1",
+          "net.caif0.dns2",
           "net.rmnet0.dns1",
           "net.rmnet0.dns2",
           "net.eth0.dns1",

diff --git a/core/java/android/os/NetStat.java b/core/java/android/os/NetStat.java
index e294cdf..b41774f 100644
--- a/core/java/android/os/NetStat.java
+++ b/core/java/android/os/NetStat.java
@@ -149,7 +149,7 @@ public class NetStat {
         // to the previous charset conversion that happened before we
         // were reusing File instances.
         File[] files = new File[2];
-        files[0] = new File("/sys/class/net/rmnet0/statistics/" + whatStat);
+        files[0] = new File("/sys/class/net/caif0/statistics/" + whatStat);
         files[1] = new File("/sys/class/net/ppp0/statistics/" + whatStat);
         return files;
     }

project system/core/
diff --git a/init/property_service.c b/init/property_service.c
index 0bc6239..eb4e4fc 100644
--- a/init/property_service.c
+++ b/init/property_service.c
@@ -53,6 +53,7 @@ struct {
     const char *prefix;
     unsigned int uid;
 } property_perms[] = {
+    { "net.caif0.",     AID_RADIO },
     { "net.rmnet0.",    AID_RADIO },
     { "net.gprs.",      AID_RADIO },
     { "ril.",           AID_RADIO },

